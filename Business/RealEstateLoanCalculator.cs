﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRealEstate
{
    public class RealEstateLoanCalculator
    {

        private LoanDuration loanDuration;
        private BorrowerInsurance insurance;
        private NominalRate rate;
        private uint capital;

        private decimal baseNumerator;
        private decimal baseDenominator;
        private decimal baseInsuranceMonthly;

        private const int HUNDRED = 100;
        private const int MONTHS_IN_YEAR = 12;

        public RealEstateLoanCalculator(LoanDuration duration, BorrowerInsurance insurance, NominalRate rate, uint capital)
        {
            this.loanDuration = duration;
            this.insurance = insurance;
            this.rate = rate;
            this.capital = capital;

            baseNumerator = (capital * ((rate / HUNDRED) / MONTHS_IN_YEAR));
            baseDenominator = (decimal)(1 - Math.Pow((double)(1 + ((rate / HUNDRED) / MONTHS_IN_YEAR)),
                - loanDuration));
            baseInsuranceMonthly = capital * ((insurance.Rate / HUNDRED) / MONTHS_IN_YEAR);
        }

        public decimal MonthlyPayment()
        {
            return Decimal.Round(baseNumerator / baseDenominator);
        }

        public decimal GlobalMonthlyPayment()
        {
            return Decimal.Round((baseNumerator / baseDenominator) + baseInsuranceMonthly);
        }

        public decimal InsuranceMonthlyPayment()
        {
            return Decimal.Round(baseInsuranceMonthly);
        }

        public decimal TotalPayment()
        {
            return Decimal.Round(
                (loanDuration * MonthlyPayment()) - capital
            );
        }

        public decimal TotalInsurance()
        {
            return Decimal.Round(baseInsuranceMonthly * loanDuration);
        }

        public decimal RepayedByTenYears()
        {
            return GlobalMonthlyPayment() * 10 * MONTHS_IN_YEAR;
        }
    }
}
