﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRealEstate;

public class NominalRate
{
    public NominalRate(decimal value)
    {
        this.Value = value;
    }

    override public string ToString()
    {
        return Value.ToString();
    }

    public static implicit operator NominalRate(decimal value) => new(value);
    public static implicit operator decimal(NominalRate value) => value.Value;

    decimal Value { get; }
}
