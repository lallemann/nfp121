﻿using BusinessRealEstate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRealEstate
{
    public class BorrowerInsurance
    {

        public const decimal BASE_RATE = 0.3m;

        public List<BorrowerTrait> Traits = new List<BorrowerTrait>();

        public decimal Rate { get; private set; }

        public BorrowerInsurance()
        {
            Rate = BASE_RATE;
            Traits.Add(new BorrowerTrait("Sporty", -0.05m));
            Traits.Add(new BorrowerTrait("Smoker", 0.15m));
            Traits.Add(new BorrowerTrait("Cardiac", 0.3m));
            Traits.Add(new BorrowerTrait("Engineer", -0.05m));
            Traits.Add(new BorrowerTrait("Pilot", 0.15m));
        }

        public List<string> GetTraitsNames()
        {
            var traits = new List<string>();
            Traits.ForEach(trait => traits.Add(trait.Name));
            return traits;
        }

        public void SetTraitActive(string traitName, bool isActive)
        {
            BorrowerTrait selectedTrait = Traits.First(trait => trait.Name == traitName);
            
            if (selectedTrait == null)
            {
                throw new KeyNotFoundException(traitName + " is not an available trait.");
            }

            if (selectedTrait.IsActive != isActive)
            {
                Rate = isActive ? Rate += selectedTrait.Rate : Rate -= selectedTrait.Rate;
                selectedTrait.IsActive = isActive;
            }
            else
            {
                throw new ArgumentException(traitName + " is already set to " + isActive.ToString());
            }
        }

    }
}
