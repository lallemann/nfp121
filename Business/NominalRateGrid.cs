﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRealEstate
{
    public class NominalRateGrid
    {
        private Dictionary<NominalRateType, NominalRate[]> nominalRates = new Dictionary<NominalRateType, NominalRate[]>();

        public NominalRateGrid()
        {
            nominalRates.Add(NominalRateType.Good,      new NominalRate[] { 0.67m, 0.85m, 1.04m, 1.27m });
            nominalRates.Add(NominalRateType.Great,     new NominalRate[] { 0.55m, 0.73m, 0.91m, 1.15m });
            nominalRates.Add(NominalRateType.Excellent, new NominalRate[] { 0.45m, 0.58m, 0.73m, 0.89m });
        }

        public NominalRate GetRate(NominalRateType type, LoanDuration duration)
        {
            NominalRate[]? ratesFromType;

            if (nominalRates.TryGetValue(type, out ratesFromType))
            {
                return ratesFromType[GetGridIndex(duration)];
            }
            else
            {
                throw new ArgumentException("Nominal rate type not specified in the grid.");
            }
        }

        private int GetGridIndex(LoanDuration durationMonths)
        {
            uint duration = durationMonths.ToYears();
            if (duration <= 10)
            {
                return 0;
            }
            else if (duration <= 15)
            {
                return 1;
            }
            else if (duration <= 20)
            {
                return 2;
            }
            else if (duration <= 25)
            {
                return 3;
            }
            else
            {
                return 3;
            }
        }
    }
}
