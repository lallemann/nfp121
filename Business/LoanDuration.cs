﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRealEstate
{
    public class LoanDuration
    {
        private const uint YEARS_TO_MONTHS = 12;
        public const uint MIN_DURATION = 9 * YEARS_TO_MONTHS;
        public const uint MAX_DURATION = 25 * YEARS_TO_MONTHS;

        public LoanDuration(uint months)
        {
            if (months < MIN_DURATION || months > MAX_DURATION)
            {
                throw new ArgumentOutOfRangeException(
                    String.Format("The loan must span between {0} and {1} months.", MIN_DURATION, MAX_DURATION));
            }

            this.Value = months;
        }

        public static implicit operator LoanDuration(uint months) => new LoanDuration(months);
        public static implicit operator uint(LoanDuration months) => months.Value;

        public uint Value { get; set; }

        public uint ToYears()
        {
            return this.Value / YEARS_TO_MONTHS;
        }
    }
}
