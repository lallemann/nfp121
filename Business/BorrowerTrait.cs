﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRealEstate
{
    public class BorrowerTrait
    {
        public string Name;

        public decimal Rate;

        public bool IsActive = false;

        public BorrowerTrait(string name, decimal rate)
        {
            this.Name = name;
            this.Rate = rate;
        }
    }
}
