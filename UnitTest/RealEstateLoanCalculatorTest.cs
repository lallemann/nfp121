﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using BusinessRealEstate;

namespace UnitTest
{
    public class RealEstateLoanCalculatorTest
    {

        private LoanDuration loanDuration = new(300);
        private BorrowerInsurance insurance = new BorrowerInsurance();
        private NominalRate rate = new(1.27m);
        private uint capital = 175000;
        private RealEstateLoanCalculator calculator;

        public RealEstateLoanCalculatorTest()
        {
            insurance.SetTraitActive("Engineer", true);
            insurance.SetTraitActive("Smoker", true);
            insurance.SetTraitActive("Cardiac", true);

            calculator = new RealEstateLoanCalculator(loanDuration, insurance, rate, capital);
        }

        [Fact]
        public void TestGlobalMonthlyPayment()
        {
            Assert.Equal(783, calculator.GlobalMonthlyPayment());
        }

        [Fact]
        public void TestInsuranceMonthlyPayment()
        {
            Assert.Equal(102, calculator.InsuranceMonthlyPayment());
        }

        [Fact]
        public void TestTotalPayment()
        {
            Assert.Equal(59966, calculator.TotalPayment());
        }

        [Fact]
        public void TestTotalInsurance()
        {
            Assert.Equal(30625, calculator.TotalInsurance());
        }

        [Fact]
        public void TestRepayedByTenYears()
        {
            Assert.Equal(93960, calculator.RepayedByTenYears());
        }
    }
}
