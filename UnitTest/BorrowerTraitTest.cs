﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using BusinessRealEstate;

namespace UnitTest
{
    public class BorrowerTraitTest
    {
        [Fact]
        public void Constructor()
        {
            const string name = "Trait1";
            const decimal rate = 1m;
            var borrowerTrait = new BorrowerTrait(name, rate);

            Assert.Equal(name, borrowerTrait.Name);
            Assert.Equal<decimal>(rate, borrowerTrait.Rate);
        }
    }
}
