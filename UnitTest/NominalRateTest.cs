﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using BusinessRealEstate;

namespace UnitTest
{
    public class NominalRateTest
    {
        [Fact]
        public void Constructor()
        {
            NominalRate rate = 0.5m;
            Assert.Equal<decimal>(0.5m, rate);
        }

        [Fact]
        public void TestToString()
        {
            NominalRate rate = 0.5m;
            const string rateStr = "0,5";
            Assert.Equal(rateStr, rate.ToString());
        }

        [Fact]
        public void FailToString()
        {
            NominalRate rate = 0.5m;
            const string rateStr = "0,50";
            Assert.NotEqual(rateStr, rate.ToString());
        }
    }
}
