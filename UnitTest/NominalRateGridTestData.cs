﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using BusinessRealEstate;
using System.Collections;

namespace UnitTest
{
    public class NominalRateGridTestData : IEnumerable<object[]>
    {

        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] { NominalRateType.Good,   new LoanDuration(10 * 12), new NominalRate(0.67m) };
                yield return new object[] { NominalRateType.Great,  new LoanDuration(14 * 12), new NominalRate(0.73m) };
                yield return new object[] { NominalRateType.Great,  new LoanDuration(15 * 12), new NominalRate(0.73m) };
                yield return new object[] { NominalRateType.Good,   new LoanDuration(25 * 12), new NominalRate(1.27m) };
            }
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            return TestData.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)TestData).GetEnumerator();
        }
    }
}
