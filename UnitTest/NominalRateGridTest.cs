﻿using BusinessRealEstate;
using System;
using Xunit;

namespace UnitTest
{
    public class NominalRateGridTest
    {
        [Theory]
        [MemberData(nameof(NominalRateGridTestData.TestData), MemberType = typeof(NominalRateGridTestData))]
        public void Constructor(NominalRateType type, LoanDuration duration, NominalRate result)
        {
            var grid = new NominalRateGrid();
            Assert.Equal(result, grid.GetRate(type, duration), 2);
        }
    }
}
