﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using BusinessRealEstate;

namespace UnitTest
{
    public class LoanDurationTest
    {
        [Fact]
        public void FailingOutOfRange()
        {
            LoanDuration duration;
            Assert.Throws<ArgumentOutOfRangeException>(() => duration = LoanDuration.MIN_DURATION - 1);
            Assert.Throws<ArgumentOutOfRangeException>(() => duration = LoanDuration.MAX_DURATION + 1);
        }

        [Fact]
        public void Constructor()
        {
            LoanDuration duration = 300;
            Assert.Equal<uint>(300, duration);
        }

        [Fact]
        public void ToYears()
        {
            LoanDuration duration = 300;
            Assert.Equal<uint>(duration / 12, duration.ToYears());
        }
    }
}
