using BusinessRealEstate;

namespace NFP121
{
    public partial class MainForm : Form
    {

        private BorrowerInsurance borrowerInsurance = new BorrowerInsurance();
        private NominalRateGrid nominalRateGrid = new NominalRateGrid();
        private NominalRate nominalRate = 0;
        private LoanDuration duration = LoanDuration.MIN_DURATION;
        private uint capital = 0;

        public MainForm()
        {
            InitializeComponent();

            // Populate Borrower Traits CheckedListBox
            lbBorrowerTraits.Items.AddRange(borrowerInsurance.GetTraitsNames().ToArray());

            // Populate Nominal Rate Types ComboBox
            cboNominalRateType.DataSource = Enum.GetNames(typeof(NominalRateType));
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            capital = (uint)nudCapital.Value;

            UpdateInsuranceRateLabel();
            UpdateNominalRate();
            UpdateCalculatedValues();
        }

        private void lbBorrowerTraits_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var senderArgs = (CheckedListBox)sender;
            bool isItemChecked = e.NewValue == CheckState.Checked ? true : false;

            try
            {
                borrowerInsurance.SetTraitActive(senderArgs.SelectedItem.ToString() ?? "", isItemChecked);
            }
            catch (KeyNotFoundException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
            }

            UpdateInsuranceRateLabel();
            UpdateCalculatedValues();
        }

        private void UpdateInsuranceRateLabel()
        {
            lblInsuranceRate.Text = borrowerInsurance.Rate.ToString();
        }

        private void nudDuration_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                duration = (uint)nudDuration.Value;
                UpdateNominalRate();
                UpdateCalculatedValues();
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void nudCapital_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                capital = (uint)nudCapital.Value;
                UpdateCalculatedValues();
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void cboNominalRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateNominalRate();
            UpdateCalculatedValues();
        }

        private void UpdateNominalRate()
        {
            string selectedText = cboNominalRateType.Text ?? "";
            try
            {
                NominalRateType type = (NominalRateType)Enum.Parse(typeof(NominalRateType), selectedText);
                decimal value = nudDuration.Value;
                LoanDuration duration = (uint)value;
                nominalRate = nominalRateGrid.GetRate(type, duration);
                lblNominalRate.Text = nominalRate.ToString();

                UpdateCalculatedValues();
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private void UpdateCalculatedValues()
        {
            var calculator = new RealEstateLoanCalculator(
                duration, borrowerInsurance, nominalRate, capital);

            lblQ1.Text = calculator.GlobalMonthlyPayment().ToString();
            lblQ2.Text = calculator.InsuranceMonthlyPayment().ToString();
            lblQ3.Text = calculator.TotalPayment().ToString();
            lblQ4.Text = calculator.TotalInsurance().ToString();
            lblQ5.Text = calculator.RepayedByTenYears().ToString();
        }
    }
}