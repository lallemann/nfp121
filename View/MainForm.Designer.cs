﻿namespace NFP121
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbBorrowerTraits = new System.Windows.Forms.CheckedListBox();
            this.lblInsuranceRate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudDuration = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nudCapital = new System.Windows.Forms.NumericUpDown();
            this.cboNominalRateType = new System.Windows.Forms.ComboBox();
            this.lblNominalRate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblQ1 = new System.Windows.Forms.Label();
            this.lblQ2 = new System.Windows.Forms.Label();
            this.lblQ3 = new System.Windows.Forms.Label();
            this.lblQ4 = new System.Windows.Forms.Label();
            this.lblQ5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCapital)).BeginInit();
            this.SuspendLayout();
            // 
            // lbBorrowerTraits
            // 
            this.lbBorrowerTraits.FormattingEnabled = true;
            this.lbBorrowerTraits.Location = new System.Drawing.Point(622, 84);
            this.lbBorrowerTraits.Name = "lbBorrowerTraits";
            this.lbBorrowerTraits.Size = new System.Drawing.Size(120, 130);
            this.lbBorrowerTraits.TabIndex = 1;
            this.lbBorrowerTraits.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lbBorrowerTraits_ItemCheck);
            // 
            // lblInsuranceRate
            // 
            this.lblInsuranceRate.AutoSize = true;
            this.lblInsuranceRate.Location = new System.Drawing.Point(729, 53);
            this.lblInsuranceRate.Name = "lblInsuranceRate";
            this.lblInsuranceRate.Size = new System.Drawing.Size(13, 15);
            this.lblInsuranceRate.TabIndex = 3;
            this.lblInsuranceRate.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(621, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Taux d\'assurance :";
            // 
            // nudDuration
            // 
            this.nudDuration.Location = new System.Drawing.Point(120, 48);
            this.nudDuration.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudDuration.Minimum = new decimal(new int[] {
            108,
            0,
            0,
            0});
            this.nudDuration.Name = "nudDuration";
            this.nudDuration.Size = new System.Drawing.Size(52, 23);
            this.nudDuration.TabIndex = 5;
            this.nudDuration.Value = new decimal(new int[] {
            108,
            0,
            0,
            0});
            this.nudDuration.ValueChanged += new System.EventHandler(this.nudDuration_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Durée du prêt :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "mois";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(524, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 15);
            this.label4.TabIndex = 10;
            this.label4.Text = "€";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(325, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "Capital emprunté :";
            // 
            // nudCapital
            // 
            this.nudCapital.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudCapital.Location = new System.Drawing.Point(436, 79);
            this.nudCapital.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudCapital.Minimum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudCapital.Name = "nudCapital";
            this.nudCapital.Size = new System.Drawing.Size(82, 23);
            this.nudCapital.TabIndex = 8;
            this.nudCapital.Value = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudCapital.ValueChanged += new System.EventHandler(this.nudCapital_ValueChanged);
            // 
            // cboNominalRateType
            // 
            this.cboNominalRateType.FormattingEnabled = true;
            this.cboNominalRateType.Location = new System.Drawing.Point(120, 84);
            this.cboNominalRateType.Name = "cboNominalRateType";
            this.cboNominalRateType.Size = new System.Drawing.Size(121, 23);
            this.cboNominalRateType.TabIndex = 11;
            this.cboNominalRateType.SelectedIndexChanged += new System.EventHandler(this.cboNominalRateType_SelectedIndexChanged);
            // 
            // lblNominalRate
            // 
            this.lblNominalRate.AutoSize = true;
            this.lblNominalRate.Location = new System.Drawing.Point(122, 134);
            this.lblNominalRate.Name = "lblNominalRate";
            this.lblNominalRate.Size = new System.Drawing.Size(89, 15);
            this.lblNominalRate.TabIndex = 12;
            this.lblNominalRate.Text = "lblNominalRate";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 13;
            this.label6.Text = "Type de taux :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Taux nominal :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(158, 15);
            this.label8.TabIndex = 15;
            this.label8.Text = "Prix global de la mensualité :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 212);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(189, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Cotisation mensuelle d’assurance :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Montant total :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(35, 299);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(219, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "Montant du capital remboursé à 10 ans :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(35, 269);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 15);
            this.label12.TabIndex = 19;
            this.label12.Text = "Assurance totale :";
            // 
            // lblQ1
            // 
            this.lblQ1.AutoSize = true;
            this.lblQ1.Location = new System.Drawing.Point(211, 185);
            this.lblQ1.Name = "lblQ1";
            this.lblQ1.Size = new System.Drawing.Size(35, 15);
            this.lblQ1.TabIndex = 20;
            this.lblQ1.Text = "lblQ1";
            // 
            // lblQ2
            // 
            this.lblQ2.AutoSize = true;
            this.lblQ2.Location = new System.Drawing.Point(239, 212);
            this.lblQ2.Name = "lblQ2";
            this.lblQ2.Size = new System.Drawing.Size(35, 15);
            this.lblQ2.TabIndex = 21;
            this.lblQ2.Text = "lblQ2";
            // 
            // lblQ3
            // 
            this.lblQ3.AutoSize = true;
            this.lblQ3.Location = new System.Drawing.Point(128, 240);
            this.lblQ3.Name = "lblQ3";
            this.lblQ3.Size = new System.Drawing.Size(35, 15);
            this.lblQ3.TabIndex = 22;
            this.lblQ3.Text = "lblQ3";
            // 
            // lblQ4
            // 
            this.lblQ4.AutoSize = true;
            this.lblQ4.Location = new System.Drawing.Point(141, 269);
            this.lblQ4.Name = "lblQ4";
            this.lblQ4.Size = new System.Drawing.Size(35, 15);
            this.lblQ4.TabIndex = 23;
            this.lblQ4.Text = "lblQ4";
            // 
            // lblQ5
            // 
            this.lblQ5.AutoSize = true;
            this.lblQ5.Location = new System.Drawing.Point(260, 299);
            this.lblQ5.Name = "lblQ5";
            this.lblQ5.Size = new System.Drawing.Size(35, 15);
            this.lblQ5.TabIndex = 24;
            this.lblQ5.Text = "lblQ5";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblQ5);
            this.Controls.Add(this.lblQ4);
            this.Controls.Add(this.lblQ3);
            this.Controls.Add(this.lblQ2);
            this.Controls.Add(this.lblQ1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblNominalRate);
            this.Controls.Add(this.cboNominalRateType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudCapital);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudDuration);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblInsuranceRate);
            this.Controls.Add(this.lbBorrowerTraits);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCapital)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CheckedListBox lbBorrowerTraits;
        private Label lblInsuranceRate;
        private Label label1;
        private NumericUpDown nudDuration;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private NumericUpDown nudCapital;
        private ComboBox cboNominalRateType;
        private Label lblNominalRate;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label lblQ1;
        private Label lblQ2;
        private Label lblQ3;
        private Label lblQ4;
        private Label lblQ5;
    }
}